-- TABLE Users
CREATE TABLE users (
    id integer PRIMARY KEY,
    first_name varchar(100) NOT NULL,
    last_name varchar(100),
    email varchar(100) NOT NULL UNIQUE,
    password varchar(255) NOT NULL,
    status smallint NOT NULL DEFAULT 0,
    online boolean DEFAULT false
);

CREATE INDEX ON users (email, password);
--CREATE INDEX name_fulltext ON users USING gin(first_name, last_name);

comment on table users is 'Пользователи системы';
comment on column users.first_name is 'Имя';
comment on column users.last_name is 'Фамилия';
comment on column users.email is 'E-mail';
comment on column users.password is 'Пароль';
comment on column users.online is 'Сейчас в системе';
comment on column users.status is 'Статус';

-- TABLE UserInfo
CREATE TABLE users_info (
    id integer PRIMARY KEY REFERENCES users(id),
    gender smallint DEFAULT 0 CONSTRAINT posible_gender CHECK (gender > -1 AND gender < 3),
    image varchar(255) DEFAULT '',
    url_www varchar(255) DEFAULT '',
    url_social varchar(255) DEFAULT ''
);

comment on table users_info is 'Дополнительная информация по Users';
comment on column users_info.gender is 'Пол';
comment on column users_info.image is 'Путь к фото файлу';
comment on column users_info.url_www is 'Ссылка на страницу';
comment on column users_info.url_social is 'Ссылка на соцсеть';

-- TABLE Meetings
CREATE TABLE meetings (
    id integer PRIMARY KEY,
    name varchar(255) NOT NULL,
    alias varchar(255) NOT NULL UNIQUE,
    content text,
    start_at TIMESTAMP WITH TIME ZONE CHECK (start_at < finish_at),
    finish_at TIMESTAMP WITH TIME ZONE CHECK (finish_at > start_at),
    id_user integer,
    FOREIGN KEY (id_user) REFERENCES users(id) ON DELETE RESTRICT,
    UNIQUE (id_user, name)
);

CREATE INDEX ON meetings (id_user, name);
CREATE INDEX ON meetings (id_user, finish_at);
CREATE INDEX ON meetings (alias);

comment on table meetings is 'Встречи/Конференции';
comment on column meetings.name is 'Название';
comment on column meetings.content is 'Описание';
comment on column meetings.start_at is 'Начало встречи';
comment on column meetings.finish_at is 'Конец встречи';
comment on column meetings.id_user is 'Создатель встречи';

-- TABLE Members
CREATE TABLE members (
    id integer PRIMARY KEY,
    id_user integer REFERENCES users(id) ON DELETE NO ACTION,
    id_meeting integer REFERENCES meetings(id) ON DELETE CASCADE,
    UNIQUE (id_user, id_meeting)
);

CREATE INDEX ON members (id_meeting);
CREATE INDEX ON members (id_user);

comment on table members is 'Участники встречи';
comment on column members.id_user is 'Пользователь';
comment on column members.id_meeting is 'Встреча';

-- TABLE Teams
CREATE TABLE teams (
    id integer PRIMARY KEY,
    name varchar(255) NOT NULL,
    id_user integer REFERENCES users(id) ON DELETE CASCADE,
    UNIQUE (id_user, name)
);

CREATE INDEX ON teams (id_user);

comment on table teams is 'Команды';
comment on column teams.name is 'Название';
comment on column teams.id_user is 'Автор';

-- TABLE TeamsUsers
CREATE TABLE teams_users (
    id integer PRIMARY KEY,
    id_user integer REFERENCES users(id) ON DELETE CASCADE,
    id_team integer REFERENCES teams(id) ON DELETE CASCADE,
    UNIQUE (id_user, id_team)
);

CREATE INDEX ON teams_users (id_team);

comment on table teams_users is 'Пользователи-команды';
comment on column teams_users.id_user is 'Пользователь';
comment on column teams_users.id_team is 'Команда';

-- TABLE Posts
CREATE TABLE posts (
    id integer PRIMARY KEY,
    content text,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    id_user integer REFERENCES users(id) ON DELETE NO ACTION,
    id_meeting integer REFERENCES meetings(id) ON DELETE NO ACTION
);

CREATE INDEX ON posts (id_meeting);
--CREATE INDEX content_fulltext ON posts USING gin(id_meeting, content);

comment on table posts is 'Посты (чат)';
comment on column posts.content is 'Текст';
comment on column posts.created_at is 'Дата создания';
comment on column posts.id_user is 'Пользователь';
comment on column posts.id_meeting is 'Встреча';

-- TABLE Tasks
CREATE TABLE tasks (
    id integer PRIMARY KEY,
    name varchar(255) NOT NULL,
    status smallint NOT NULL DEFAULT 0,
    content json NOT NULL,
    id_meeting integer REFERENCES meetings(id) ON DELETE CASCADE,
    UNIQUE (id_meeting, name)
);

CREATE INDEX ON tasks (id_meeting, status);

comment on table tasks is 'Задачи в встрече';
comment on column tasks.name is 'Название';
comment on column tasks.status is 'Статус';
comment on column tasks.content is 'Содержание';
comment on column tasks.id_meeting is 'Встреча';

-- TABLE Docs
CREATE TABLE docs (
    id integer PRIMARY KEY,
    name varchar(255) NOT NULL,
    filename varchar(255) NOT NULL,
    id_meeting integer REFERENCES meetings(id) ON DELETE CASCADE,
    UNIQUE (id_meeting, name, filename)
);

CREATE INDEX ON docs (id_meeting);

comment on table docs is 'Материалы к встрече';
comment on column docs.name is 'Название';
comment on column docs.filename is 'Путь к файлу';
comment on column docs.id_meeting is 'Встреча';

-- TABLE Reports
CREATE TABLE reports (
    id integer PRIMARY KEY,
    content text,
    users_visited json,
    users_novisited json,
    id_meeting integer REFERENCES meetings(id) ON DELETE CASCADE
);

CREATE INDEX ON reports (id_meeting);

comment on table reports is 'Отчет по встрече';
comment on column reports.content is 'Содержание';
comment on column reports.users_visited is 'Какие пользователи посетили встречу';
comment on column reports.users_novisited is 'Какие пользователи пропустили встречу';
comment on column reports.id_meeting is 'Встреча';