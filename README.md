# Курс БД. Домашнее задание №2
## Проектирование проекта «Он-лайн встречи(конференции)». Индексы и ограничения

## Схема
![](entities.png?raw=true)

Схема с комментариями [Открыть](./entities-comment.png).

Полный дамп БД для postgres [открыть](./dump.sql)

## Документация
Приложение для проведения быстрых он-лайн встреч(конференций).


## Сущности
### Таблица Users. Пользователи системы

| Название       | Тип                    | Ключ  | Комментарий             |
|----------------|------------------------| ------|-------------------------|
| id             | int                    | PK    |                         |
| first_name     | character varying(100) | -     | Имя                     |
| last_name      | character varying(100) | -     | Фамилия                 |
| email          | character varying(100) | -     | E-mail                  |
| password       | character varying(255) | -     | Пароль                  |
| status         | smallint               | -     | Статус                  |
| online         | boolean                | -     | Находиться ли в системе |

``` sql
CREATE TABLE users (
    id integer PRIMARY KEY,
    first_name varchar(100) NOT NULL,
    last_name varchar(100),
    email varchar(100) NOT NULL UNIQUE,
    password varchar(255) NOT NULL,
    status smallint NOT NULL DEFAULT 0,
    online boolean DEFAULT false
);

CREATE INDEX ON users (email, password);
--CREATE INDEX name_fulltext ON users USING gin(first_name, last_name);
````

Бизнес-процессы (запросы):
- Получение (CRUD) пользователя по id.
- Получение пользователей (сортировка по id).
- Авторизация пользователя по email и password.
- Поиск пользователя по имени и фамилии.

Индексы, ограничения:
- PRIMARY KEY id. Получение по id, сортировка по id.
- INDEX email, password. Для Авторизации. 
- INDEX name_fulltext по полям first_name, last_name. Для поиска.
- UNIQUE для email. Не должно быть пользователей с одинаковым email.
- NOT NULL для first_name, email, password, status. 


### Таблица UsersInfo. Дополнительная информация по пользователям системы

| Название       | Тип                    | Ключ  | Комментарий           |
| -------------- |------------------------| ------|-----------------------|
| id             | int                    | PK    |                       |
| gender         | smallint               | -     | Пол                   |
| image          | character varying(255) | -     | Фото                  |
| url_www        | character varying(200) | -     | Ссылка на личный сайт |
| url_social     | character varying(200) | -     | Ссылка на соцсеть     |

``` sql
CREATE TABLE users_info (
    id integer PRIMARY KEY REFERENCES users(id),
    gender smallint DEFAULT 0 CONSTRAINT posible_gender CHECK (gender > -1 AND gender < 3),
    image varchar(255) DEFAULT '',
    url_www varchar(255) DEFAULT '',
    url_social varchar(255) DEFAULT ''
);
````

Бизнес-процессы (запросы):
- Получение (CRUD) пользователя по id.
- Получение пользователей (сортировка по id).
- PRIMARY KEY id. Получение по id, сортировка по id.


### Таблица Teams. Команды
> Для добавления участников встречи за раз (сформировал свою комнаду,
> нажал одну кнопку и пользователи команды в участниках).

| Название | Тип                      | Ключ  | Комментарий              |
|----------|--------------------------| ------|--------------------------|
| id       | int                      | PK    |                          |
| id_user  | int                      | FK    | Чья команда. PK Users    |
| name     | character varying(255)   | -     | Название                 |

``` sql
CREATE TABLE teams (
    id integer PRIMARY KEY,
    name varchar(255) NOT NULL,
    id_user integer REFERENCES users(id) ON DELETE CASCADE,
    UNIQUE (id_user, name)
);

CREATE INDEX ON teams (id_user);
````

Бизнес-процессы (запросы):
- Получение (CRUD) по id.
- Получение команд пользователя (по id_user, сортировка по id).

Индексы, ограничения:
- PRIMARY KEY id. Получение по id, сортировка по id.
- INDEX id_user. Для получения команд пользователя. 
- FK id_user. CASCADE на удаление.
- UNIQUE для id_user, name. Один пользователь не может иметь одинаковые команды.
- NOT NULL для name. 
- CONSTRAINT posible_gender CHECK (gender > -1 AND gender < 3). Пол может быть мужской (2), женский (1) и не указан (0).


### Таблица TeamsUser. 
> Связка пользователей и команд

| Название | Тип                      | Ключ  | Комментарий              |
|----------|--------------------------| ------|--------------------------|
| id       | int                      | PK    |                          |
| id_user  | int                      | FK    | PK Users                 |
| id_team  | int                      | FK    | PK Teams                 |

``` sql
CREATE TABLE teams_users (
    id integer PRIMARY KEY,
    id_user integer REFERENCES users(id) ON DELETE CASCADE,
    id_team integer REFERENCES teams(id) ON DELETE CASCADE,
    UNIQUE (id_user, id_team)
);

CREATE INDEX ON teams_users (id_team);
````

Бизнес-процессы (запросы):
- Получение (CRUD) по id.
- Получение набора (по id_team, сортировка по id).

Индексы, ограничения:
- PRIMARY KEY id. Получение по id, сортировка по id.
- INDEX id_team. Для получения набора. 
- FK id_user. CASCADE на удаление.
- FK id_team. CASCADE на удаление.
- UNIQUE для id_user, id_team. Не повторяющиеся записи.


### Таблица Meetings. Встречи (конференции и т.п)
| Название  | Тип                    | Ключ  | Комментарий                 |
|-----------|------------------------| ------|-----------------------------|
| id        | int                    | PK    |                             |
| id_user   | int                    | FK    | PK Users                    |
| name      | character varying(100) | -     | Название                    |
| alias     | character varying(255) | -     | Название на латинском       |
| content   | text                   | -     | Описание                    |
| start_at  | timestamp              | -     | Дата старта                 |
| finish_at | timestamp              | -     | Дата окончания              |

``` sql
CREATE TABLE meetings (
    id integer PRIMARY KEY,
    name varchar(255) NOT NULL,
    alias varchar(255) NOT NULL UNIQUE,
    content text,
    start_at TIMESTAMP WITH TIME ZONE CHECK (start_at < finish_at),
    finish_at TIMESTAMP WITH TIME ZONE CHECK (finish_at > start_at),
    id_user integer,
    FOREIGN KEY (id_user) REFERENCES users(id) ON DELETE RESTRICT,
    UNIQUE (id_user, name)
);

CREATE INDEX ON meetings (id_user, name);
CREATE INDEX ON meetings (id_user, finish_at);
CREATE INDEX ON meetings (alias);
````

Бизнес-процессы (запросы):
- Получение (CRUD) по id.
- Получение по alias.
- Получение встреч пользователя (по id_user, сортировка по id).
- Поиск по id_user, name.
- Поиск по id_user, finish_at.

Индексы, ограничения:
- PRIMARY KEY id. Получение по id, сортировка по id.
- INDEX alias. Для получения встречи по его уникальному имени.
- INDEX id_user, name. Для получения/поиска встреч.
- INDEX id_user, finish_at. Для получения актуальных встреч. 
- UNIQUE alias.
- UNIQUE для id_user, name. У пользователя не может быть одинаковых встреч.
- NOT NULL для name, alias, status. 
- CHECK (start_at < finish_at).
- CHECK (finish_at > start_at).
- RESTRICT на удаление users.


### Таблица Members. Участники Meetings
| Название       | Тип                      | Ключ  | Комментарий                 |
| -------------- |--------------------------| ------|-----------------------------|
| id             | int                      | PK    |                             |
| id_user        | int                      | FK    | PK Users                    |
| id_meeting     | int                      | FK    | PK Meetings                 |

``` sql
CREATE TABLE members (
    id integer PRIMARY KEY,
    id_user integer REFERENCES users(id) ON DELETE NO ACTION,
    id_meeting integer REFERENCES meetings(id) ON DELETE CASCADE,
    UNIQUE (id_user, id_meeting)
);

CREATE INDEX ON members (id_meeting);
CREATE INDEX ON members (id_user);
````

Бизнес-процессы (запросы):
- Получение (CRUD) по id.
- Получение участников (по id_meeting, сортировка по id).
- Получение встреч (по id_user, сортировка по id).

Индексы, ограничения:
- PRIMARY KEY id. Получение по id, сортировка по id.
- INDEX id_meeting. Для получения участников встречи. 
- INDEX id_user. Для получения встреч участника. 
- FK id_user. NO ACTION на удаление.
- FK id_meeting. CASCADE на удаление.
- UNIQUE для id_user, id_meeting. Не повторяющиеся записи.


### Таблица Tasks. Задачи к Meetings
| Название       | Тип                      | Ключ  | Комментарий      |
| -------------- |--------------------------| ------|------------------|
| id             | int                      | PK    |                  |
| id_meeting     | int                      | FK    | PK Meetings      |
| name           | character varying(255)   | -     | Название         |
| status         | smallint                 | -     | Статус           |
| content        | text                     | -     | Содержание/текст |

``` sql
CREATE TABLE tasks (
    id integer PRIMARY KEY,
    name varchar(255) NOT NULL,
    status smallint NOT NULL DEFAULT 0,
    content json NOT NULL,
    id_meeting integer REFERENCES meetings(id) ON DELETE CASCADE,
    UNIQUE (id_meeting, name)
);

CREATE INDEX ON tasks (id_meeting, status);
````

Бизнес-процессы (запросы):
- Получение (CRUD) по id.
- Получение задач (по id_meeting, сортировка по id).
- Получение задач по статусу (по id_meeting и status, сортировка по id).

Индексы, ограничения:
- PRIMARY KEY id. Получение по id, сортировка по id.
- INDEX id_meeting. Для получения  задач. 
- FK id_meeting. CASCADE на удаление.
- NOT NULL для name, content, status.
- UNIQUE для id_meeting, name. У одной встречи не могут повторятся задачи.


### Таблица Docs. Материалы к Meetings
| Название       | Тип                      | Ключ  | Комментарий                 |
| -------------- |--------------------------| ------|-----------------------------|
| id             | int                      | PK    |                             |
| id_meeting     | int                      | FK    | PK Meetings                 |
| name           | character varying(255)   | -     | Название                    |
| filename       | character varying(255)   | -     | Файл                        |

``` sql
CREATE TABLE docs (
    id integer PRIMARY KEY,
    name varchar(255) NOT NULL,
    filename varchar(255) NOT NULL,
    id_meeting integer REFERENCES meetings(id) ON DELETE CASCADE,
    UNIQUE (id_meeting, name, filename)
);

CREATE INDEX ON docs (id_meeting);
````

Бизнес-процессы (запросы):
- Получение (CRUD) по id.
- Получение документов (по id_meeting, сортировка по id).

Индексы, ограничения:
- PRIMARY KEY id. Получение по id, сортировка по id.
- INDEX id_meeting. Для получения  документов. 
- FK id_meeting. CASCADE на удаление.
- NOT NULL для name, filename.
- UNIQUE для id_meeting, name, filename. Не должно быть одинаковых материалов у встречи.


### Таблица Reports. Отчет к Meetings
| Название         | Тип                      | Ключ  | Комментарий                   |
|------------------|--------------------------| ------|-------------------------------|
| id               | int                      | PK    |                               |
| id_meeting       | int                      | FK    | PK Meetings                   |
| users_visited    | json                     | -     | Какие пользователи посетили   |
| users_novisited  | json                     | -     | Какие пользователи пропустили |
| content          | text                     | -     | Содержание/текст              |

``` sql
CREATE TABLE reports (
    id integer PRIMARY KEY,
    content text,
    users_visited json,
    users_novisited json,
    id_meeting integer REFERENCES meetings(id) ON DELETE CASCADE
);

CREATE INDEX ON reports (id_meeting);
````

Бизнес-процессы (запросы):
- Получение (CRUD) по id.
- Получение отчета (по id_meeting, сортировка по id).

Индексы, ограничения:
- PRIMARY KEY id. Получение по id, сортировка по id.
- INDEX id_meeting. Для получения  отчета. 
- FK id_meeting. CASCADE на удаление.


### Таблица Posts. Чат в Meetings.
| Название       | Тип                  | Ключ  | Комментарий                 |
| -------------- |----------------------| ------|-----------------------------|
| id             | int                  | PK    |                             |
| id_meeting     | int                  | FK    | PK Meetings                 |
| id_user        | int                  | FK    | PK Users                    |
| content        | text                 | -     | Текст                       |
| created_at     | timestamp            | -     | Дата создания               |

``` sql
CREATE TABLE posts (
    id integer PRIMARY KEY,
    content text,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    id_user integer REFERENCES users(id) ON DELETE NO ACTION,
    id_meeting integer REFERENCES meetings(id) ON DELETE NO ACTION
);

CREATE INDEX ON posts (id_meeting);
--CREATE INDEX content_fulltext ON posts USING gin(id_meeting, content);
````

Бизнес-процессы (запросы):
- Получение (CRUD) по id.
- Получение постов (по id_meeting, сортировка по id).
- Полнотекстовый поиск в постах (по id_meeting, content).

Индексы, ограничения:
- PRIMARY KEY id. Получение по id, сортировка по id.
- INDEX id_meeting. Для получения  постов.
- INDEX content_fulltext. Для поиска в постах. 
- FK id_user. NO ACTION на удаление.
- FK id_meeting. NO ACTION на удаление.
- NOT NULL для created_at.